﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Xml.Linq;

namespace MainWindow
{
    public partial class Form1 : Form
    {
        enum LanguageSetting { eng, chs };
        int dinoSize = 0;
        Dictionary<string, string>[] DictDino = new Dictionary<string, string>[0];
        Dictionary<string, string>[] DictFood;
        public enum TamingMethod { Standard, NonViolent };
        LanguageSetting curLang = LanguageSetting.eng;

        public Form1()
        {
            InitializeComponent();
            FoodDictBuilder();
            DinoDictBuilder();

            //Getting system language
            System.Globalization.CultureInfo ci = System.Globalization.CultureInfo.InstalledUICulture;
            if (ci.EnglishName.StartsWith("Chinese"))
            {
                curLang = LanguageSetting.chs;
                rb_chs.Checked = true;
            }

            BuildUIText();
            cmb_creature.SelectedIndex = 0;
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        Creature creature = new Creature();

        #region builders
        private void FoodDictBuilder()
        {
            XElement xlist = XElement.Load("Data.xml");

            XName foodlist = XName.Get("FoodList");
            XName food = XName.Get("food");
            XName foodName = XName.Get("name");
            XName chnName = XName.Get("chnName");
            XName foodAmount = XName.Get("foodAmount");
            XName affinity = XName.Get("affinity");

            var foodlistnodes = xlist.Elements(foodlist);
            int foodlistsize = foodlistnodes.Elements(food).Count();
            DictFood = new Dictionary<string, string>[foodlistsize];
                        
            for (int i = 0; i < foodlistsize; i++)
            {
                DictFood[i] = new Dictionary<string, string>();
                var foodelement = foodlistnodes.Elements(food).First();
                DictFood[i].Add("Name", foodelement.Attribute(foodName).Value);
                DictFood[i].Add("chnName", foodelement.Element(chnName).Value);
                DictFood[i].Add("FoodAmount", foodelement.Element(foodAmount).Value);
                DictFood[i].Add("Affinity", foodelement.Element(affinity).Value);
                
                foodelement.Remove();
            }
        }

        private void DinoDictBuilder()
        {
            XElement xlist = XElement.Load("Data.xml");
            
            #region xNames
            XName dinolist = XName.Get("DinoList");
            XName dino = XName.Get("dino");
            XName dinoName = XName.Get("name");
            XName chnName = XName.Get("chnName");

            XName stats = XName.Get("stats");
            XName Hp = XName.Get("Hp");
            XName HpW = XName.Get("HpW");
            XName HpD = XName.Get("HpD");
            XName Stamina = XName.Get("Stamina");
            XName StaminaW = XName.Get("StaminaW");
            XName StaminaD = XName.Get("StaminaD");
            XName Oxygen = XName.Get("Oxygen");
            XName OxygenW = XName.Get("OxygenW");
            XName OxygenD = XName.Get("OxygenD");
            XName Food = XName.Get("Food");
            XName FoodW = XName.Get("FoodW");
            XName FoodD = XName.Get("FoodD");
            XName Weight = XName.Get("Weight");
            XName WeightW = XName.Get("WeightW");
            XName WeightD = XName.Get("WeightD");
            XName Melee = XName.Get("Melee");
            XName MeleeW = XName.Get("MeleeW");
            XName MeleeD = XName.Get("MeleeD");
            XName Move = XName.Get("Move");
            XName MoveW = XName.Get("MoveW");
            XName MoveD = XName.Get("MoveD");
            XName Torpor = XName.Get("Torpor");
            XName TorporW = XName.Get("TorporW");

            XName foodrate = XName.Get("foodrate");
            XName basetorporrate = XName.Get("basetorporrate");
            XName baseaffinity = XName.Get("baseaffinity");
            XName affinityperlevel = XName.Get("affinityperlevel");
            XName ineffectbyaff = XName.Get("ineffectbyaff");
            XName basefood = XName.Get("basefood");
            XName foods = XName.Get("foods");
            XName kibble = XName.Get("kibble");
            
            XName projectiledamage = XName.Get("projectiledamage");
            XName hitboxes = XName.Get("hitboxes");
            XName Body = XName.Get("Body");
            XName Head = XName.Get("Head");
            XName Shell = XName.Get("Shell");
            XName Tail = XName.Get("Tail");

            XName tamingmethods = XName.Get("tamingmethods");
            XName nonviolentfoodratemultiplier = XName.Get("nonviolentfoodratemultiplier");
            XName nonviolentfoodaffinitymultiplier = XName.Get("nonviolentfoodaffinitymultiplier");
            #endregion

            var dinolistnodes = xlist.Elements(dinolist);
            
            int dinolistsize = dinolistnodes.Elements(dino).Count();
            dinoSize = dinolistsize;
            DictDino = new Dictionary<string, string>[dinolistsize];

            for (int i = 0; i < dinolistsize; i++)
            {
                DictDino[i] = new Dictionary<string, string>();
                var dinoelement = dinolistnodes.Elements(dino).First();
                var statselement = dinoelement.Elements(stats).First();
                
                try {
                    DictDino[i].Add("name", dinoelement.Attribute(dinoName).Value);
                    DictDino[i].Add("chnName", dinoelement.Element(chnName).Value);

                    DictDino[i].Add("hp", statselement.Element(Hp).Value);
                    DictDino[i].Add("hpW", statselement.Element(HpW).Value);
                    DictDino[i].Add("hpD", statselement.Element(HpD).Value);
                    DictDino[i].Add("stamina", statselement.Element(Stamina).Value);
                    DictDino[i].Add("staminaW", statselement.Element(StaminaW).Value);
                    DictDino[i].Add("staminaD", statselement.Element(StaminaD).Value);
                    DictDino[i].Add("oxygen", statselement.Element(Oxygen).Value);
                    DictDino[i].Add("oxygenW", statselement.Element(OxygenW).Value);
                    DictDino[i].Add("oxygenD", statselement.Element(OxygenD).Value);
                    DictDino[i].Add("food", statselement.Element(Food).Value);
                    DictDino[i].Add("foodW", statselement.Element(FoodW).Value);
                    DictDino[i].Add("foodD", statselement.Element(FoodD).Value);
                    DictDino[i].Add("weight", statselement.Element(Weight).Value);
                    DictDino[i].Add("weightW", statselement.Element(WeightW).Value);
                    DictDino[i].Add("weightD", statselement.Element(WeightD).Value);
                    DictDino[i].Add("melee", statselement.Element(Melee).Value);
                    DictDino[i].Add("meleeW", statselement.Element(MeleeW).Value);
                    DictDino[i].Add("meleeD", statselement.Element(MeleeD).Value);
                    DictDino[i].Add("move", statselement.Element(Move).Value);
                    DictDino[i].Add("moveW", statselement.Element(MoveW).Value);
                    DictDino[i].Add("moveD", statselement.Element(MoveD).Value);
                    DictDino[i].Add("torpor", statselement.Element(Torpor).Value);
                    DictDino[i].Add("torporW", statselement.Element(TorporW).Value);

                    if (dinoelement.Element(basetorporrate) != null)
                        DictDino[i].Add("basetorporrate", dinoelement.Element(basetorporrate).Value);
                    DictDino[i].Add("baseaffinity", dinoelement.Element(baseaffinity).Value);
                    DictDino[i].Add("affinityperlevel", dinoelement.Element(affinityperlevel).Value);
                    DictDino[i].Add("ineffectbyaff", dinoelement.Element(ineffectbyaff).Value);
                    DictDino[i].Add("basefood", dinoelement.Element(basefood).Value);
                    DictDino[i].Add("foods", dinoelement.Element(foods).Value);
                    if (dinoelement.Element(kibble) != null)
                        DictDino[i].Add("kibble", dinoelement.Element(kibble).Value);
                    if (dinoelement.Element(projectiledamage) != null)
                        DictDino[i].Add("projectiledamage", dinoelement.Element(projectiledamage).Value);
                    if (dinoelement.Element(hitboxes) != null)
                    {
                        var hbelement = dinoelement.Elements(hitboxes).First();
                        if (hbelement.Element(Body) != null)
                            DictDino[i].Add("hb_body", hbelement.Element(Body).Value);
                        if (hbelement.Element(Head) != null)
                            DictDino[i].Add("hb_head", hbelement.Element(Head).Value);
                        if (hbelement.Element(Shell) != null)
                            DictDino[i].Add("hb_shell", hbelement.Element(Shell).Value);
                        if (hbelement.Element(Tail) != null)
                            DictDino[i].Add("hb_tail", hbelement.Element(Tail).Value);
                    }
                    DictDino[i].Add("tamingmethods", dinoelement.Element(tamingmethods).Value);
                    if (dinoelement.Element(nonviolentfoodratemultiplier) != null)
                        DictDino[i].Add("nonviolentfoodratemultiplier", dinoelement.Element(nonviolentfoodratemultiplier).Value);
                    if (dinoelement.Element(nonviolentfoodaffinitymultiplier) != null)
                        DictDino[i].Add("nonviolentfoodaffinitymultiplier", dinoelement.Element(nonviolentfoodaffinitymultiplier).Value);

                    dinoelement.Remove();
                }
                catch
                {
                    MessageBox.Show("Your Data.xml file is not coded properly. Re-download the software and try again.", "Bad xml file", MessageBoxButtons.OK);
                    break;
                }

            }

        }

        private void BuildUIText()
        {
            int curSelected = cmb_creature.SelectedIndex;
            cmb_creature.Items.Clear();
            if (curLang == LanguageSetting.eng)
            {
                foreach (var dino in DictDino)
                {
                    string dinoname;
                    dino.TryGetValue("name", out dinoname);
                    cmb_creature.Items.Add(dinoname);
                }
                BuildFoodLable();
            }
            else
            {
                foreach (var dino in DictDino)
                {
                    string dinoname;
                    dino.TryGetValue("chnName", out dinoname);
                    cmb_creature.Items.Add(dinoname);
                }
                BuildFoodLable();
            }
            cmb_creature.SelectedIndex = curSelected;
        }

        private void BuildFoodLable()
        {
            if (curLang == LanguageSetting.eng)
            {
                int l = creature.Foods.Length;
                if (l > 0)
                {
                    int i = 0;
                    foreach (Control c in tab_tame.Controls)
                    {
                        if (i >= l)
                            break;
                        if (c.GetType() == typeof(Label))
                        {
                            if (c.Name.StartsWith("lbl_v_food")&c.Name.Length==11)
                            {
                                c.Text = creature.Foods[i];
                                i++;
                            }
                        }
                    }
                }
                lbl_v_food4.Text = string.Format("Kibble: {0} egg", creature.Kibble);
            }
            else
            {
                int l = creature.Foods.Length;
                if (l > 0)
                {
                    int i = 0;
                    foreach (Control c in tab_tame.Controls)
                    {
                        if (i >= l)
                            break;
                        if (c.GetType() == typeof(Label))
                        {
                            if (c.Name.StartsWith("lbl_v_foods"))
                            {
                                c.Text = creature.Foods[i];
                                i++;
                            }
                        }
                    }
                }
                lbl_v_food4.Text = string.Format("饲料: {0}蛋", creature.Kibble);
            }
        }
        #endregion

        #region calculators
        private void SetDinoInfo()
        {
            string hp, hpW, hpD, stamina, staminaW, staminaD, oxygen, oxygenW, oxygenD, food, foodW, foodD, foodrate, weight, weightW, weightD, melee, meleeW, meleeD, move, moveW, moveD, 
                torpor, torporW, torporrate, baseaffinity, affinityperlvl, ineffectbyaff, basefood, _foods, kibble, food1, food2, food3, projectiledamage, hb_body, hb_head, hb_shell, hb_tail, tamingmethods,
                nonviolentfoodratemultiplier, nonviolentfoodaffinitymultiplier;
            string[] foods;
            for (int i = 0; i < DictDino.Length; i++)
            {
                Dictionary<string, string> item = DictDino[i];
                if (item.ContainsValue(creature.Name))
                {
                    #region list
                    if (item.TryGetValue("hp", out hp))
                        creature.Hp = Convert.ToDouble(hp);
                    if (item.TryGetValue("hpW", out hpW))
                        creature.HpW = Convert.ToDouble(hpW);
                    if (item.TryGetValue("hpW", out hpD))
                        creature.HpD = Convert.ToDouble(hpD);

                    if (item.TryGetValue("stamina", out stamina))
                        creature.Stamina = Convert.ToDouble(stamina);
                    if (item.TryGetValue("staminaW", out staminaW))
                        creature.StaminaW = Convert.ToDouble(staminaW);
                    if (item.TryGetValue("staminaD", out staminaD))
                        creature.StaminaD = Convert.ToDouble(staminaD);

                    if (item.TryGetValue("oyxgen", out oxygen))
                        creature.Oxygen = Convert.ToDouble(oxygen);
                    if (item.TryGetValue("oyxgenW", out oxygenW))
                        creature.OxygenW = Convert.ToDouble(oxygenW);
                    if (item.TryGetValue("oyxgenD", out oxygenD))
                        creature.OxygenD = Convert.ToDouble(oxygenD);

                    if (item.TryGetValue("food", out food))
                        creature.Food = Convert.ToDouble(food);
                    if (item.TryGetValue("foodW", out foodW))
                        creature.FoodW = Convert.ToDouble(foodW);
                    if (item.TryGetValue("foodD", out foodD))
                        creature.FoodD = Convert.ToDouble(foodD);
                    if (item.TryGetValue("foodrate", out foodrate))
                        creature.Foodrate = Convert.ToDouble(foodrate);

                    if (item.TryGetValue("weight", out weight))
                        creature.Weight = Convert.ToDouble(weight);
                    if (item.TryGetValue("weightW", out weightW))
                        creature.WeightW = Convert.ToDouble(weightW);
                    if (item.TryGetValue("weightD", out weightD))
                        creature.WeightD = Convert.ToDouble(weightD);

                    if (item.TryGetValue("melee", out melee))
                        creature.Melee = Convert.ToDouble(melee);
                    if (item.TryGetValue("meleeW", out meleeW))
                        creature.MeleeW = Convert.ToDouble(meleeW);
                    if (item.TryGetValue("meleeD", out meleeD))
                        creature.MeleeD = Convert.ToDouble(meleeD);

                    if (item.TryGetValue("move", out move))
                        creature.Move = Convert.ToDouble(move);
                    if (item.TryGetValue("moveW", out moveW))
                        creature.MoveW = Convert.ToDouble(moveW);
                    if (item.TryGetValue("moveD", out moveD))
                        creature.MoveD = Convert.ToDouble(moveD);

                    if (item.TryGetValue("torpor", out torpor))
                        creature.Torpor = Convert.ToDouble(torpor);
                    if (item.TryGetValue("torporW", out torporW))
                        creature.TorporW = Convert.ToDouble(torporW);
                    if (item.TryGetValue("torporrate", out torporrate))
                        creature.Torporrate = Convert.ToDouble(torporrate);

                    if (item.TryGetValue("baseaffinity", out baseaffinity))
                        creature.Baseaffinity = Convert.ToDouble(baseaffinity);
                    if (item.TryGetValue("affinityperlvl", out affinityperlvl))
                        creature.Affinityperlvl = Convert.ToDouble(affinityperlvl);
                    if (item.TryGetValue("ineffectbyaff", out ineffectbyaff))
                        creature.Ineffectbyaff = Convert.ToDouble(ineffectbyaff);

                    if (item.TryGetValue("basefood", out basefood))
                        creature.Basefood = basefood;
                    if (item.TryGetValue("foods", out _foods))
                    {
                        char[] d = new char[1] { ',' };
                        try
                        {
                            foods = _foods.Split(d);
                            creature.Foods = foods;
                        }
                        catch
                        { MessageBox.Show("Invalid food data. Edit food data in xml file and try again.", "Bad xml file", MessageBoxButtons.OK); }
                    }

                    if (item.TryGetValue("kibble", out kibble))
                    {
                        creature.Kibble = kibble;
                    }

                    if (item.TryGetValue("projectiledamage", out projectiledamage))
                        creature.Projectiledamage = Convert.ToDouble(projectiledamage);

                    if (item.TryGetValue("hb_body", out hb_body))
                        creature.Hb_body = Convert.ToDouble(hb_body);
                    if (item.TryGetValue("hb_head", out hb_head))
                        creature.Hb_head = Convert.ToDouble(hb_head);
                    if (item.TryGetValue("hb_shell", out hb_shell))
                        creature.Hb_shell = Convert.ToDouble(hb_shell);
                    if (item.TryGetValue("hb_tail", out hb_tail))
                        creature.Hb_tail = Convert.ToDouble(hb_tail);

                    if (item.TryGetValue("tamingmethods", out tamingmethods))
                    {
                        if (tamingmethods == "Standard")
                            creature.Tamingmethods = TamingMethod.Standard;
                        else
                            creature.Tamingmethods = TamingMethod.NonViolent;
                    }

                    if (item.TryGetValue("nonviolentfoodratemultiplier", out nonviolentfoodratemultiplier))
                        creature.Nonviolentfoodratemultiplier = Convert.ToDouble(nonviolentfoodratemultiplier);
                    if (item.TryGetValue("nonviolentfoodaffinitymultiplier", out nonviolentfoodaffinitymultiplier))
                        creature.Nonviolentfoodaffinitymultiplier = Convert.ToDouble(nonviolentfoodaffinitymultiplier);
                    #endregion
                    break;
                }
            }
            BuildFoodLable();
        }

        private void CalEffective(string[][] foodlist)
        {
            double eff = 100;
            int foodQ = 0;
            double foodAff = 0;
            for (int i = 0; i < foodlist.Length; i++)
            {
                foodQ = Convert.ToInt32(foodlist[i][0]);
                foodAff = Convert.ToDouble(foodlist[i][1]);
                for (int j = 1; j <= foodQ; j++)
                {
                    eff -= Math.Pow(eff, 2) * creature.Ineffectbyaff / foodAff / creature.Nonviolentfoodaffinitymultiplier / 100;
                }
            }
            creature.Effectiveness = Math.Round(eff, 1);
            creature.Extralevels = Math.Floor(creature.Level * 0.5 * eff / 100);
        }

        private void CalFood()
        {

        }

        #endregion

        private void cmb_creature_SelectedIndexChanged(object sender, EventArgs e)
        {
            creature.Name = cmb_creature.Text;
            SetDinoInfo();
        }

        private void num_lvl_ValueChanged(object sender, EventArgs e)
        {
            creature.Level = (int)num_lvl.Value;

            string[][] foodlist = new string[1][];
            foodlist[0] = new string[2];
            foodlist[0][0] = "98";
            foodlist[0][1] = "400";
            CalEffective(foodlist);
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (rb_eng.Checked)
                curLang = LanguageSetting.eng;
            else
                curLang = LanguageSetting.chs;
            BuildUIText();
        }
    }

    #region property
    public class Creature
    {
        #region stats
        private string _name = "";
        public string Name
        {
            set { _name = value; }
            get { return _name; }
        }

        private double _level = 0;
        public double Level
        {
            set { _level = value; }
            get { return _level; }
        }

        private double _hp = 0;
        public double Hp
        {
            set { _hp = value; }
            get { return _hp; }
        }

        private double _hpW = 0;
        public double HpW
        {
            set { _hpW = value; }
            get { return _hpW; }
        }

        private double _hpD = 0;
        public double HpD
        {
            set { _hpD = value; }
            get { return _hpD; }
        }

        private double _stamina = 0;
        public double Stamina
        {
            set { _stamina = value; }
            get { return _stamina; }
        }

        private double _staminaW = 0;
        public double StaminaW
        {
            set { _staminaW = value; }
            get { return _staminaW; }
        }

        private double _staminaD = 0;
        public double StaminaD
        {
            set { _staminaD = value; }
            get { return _staminaD; }
        }

        private double _oxygen = 0;
        public double Oxygen
        {
            set { _oxygen = value; }
            get { return _oxygen; }
        }

        private double _oxygenW = 0;
        public double OxygenW
        {
            set { _oxygenW = value; }
            get { return _oxygenW; }
        }

        private double _oxygenD = 0;
        public double OxygenD
        {
            set { _oxygenD = value; }
            get { return _oxygenD; }
        }

        private double _food = 0;
        public double Food
        {
            set { _food = value; }
            get { return _food; }
        }

        private double _foodW = 0;
        public double FoodW
        {
            set { _foodW = value; }
            get { return _foodW; }
        }

        private double _foodD = 0;
        public double FoodD
        {
            set { _foodD = value; }
            get { return _foodD; }
        }

        private double _foodrate = 0;
        public double Foodrate
        {
            set { _foodrate = value; }
            get { return _foodrate; }
        }

        private double _weight = 0;
        public double Weight
        {
            set { _weight = value; }
            get { return _weight; }
        }

        private double _weightW = 0;
        public double WeightW
        {
            set { _weightW = value; }
            get { return _weightW; }
        }

        private double _weightD = 0;
        public double WeightD
        {
            set { _weightD = value; }
            get { return _weightD; }
        }

        private double _melee = 0;
        public double Melee
        {
            set { _melee = value; }
            get { return _melee; }
        }

        private double _meleeW = 0;
        public double MeleeW
        {
            set { _meleeW = value; }
            get { return _meleeW; }
        }

        private double _meleeD = 0;
        public double MeleeD
        {
            set { _meleeD = value; }
            get { return _meleeD; }
        }

        private double _move = 0;
        public double Move
        {
            set { _move = value; }
            get { return _move; }
        }

        private double _moveW = 0;
        public double MoveW
        {
            set { _moveW = value; }
            get { return _moveW; }
        }

        private double _moveD = 0;
        public double MoveD
        {
            set { _moveD = value; }
            get { return _moveD; }
        }

        private double _torpor = 0;
        public double Torpor
        {
            set { _torpor = value; }
            get { return _torpor; }
        }

        private double _torporW = 0;
        public double TorporW
        {
            set { _torporW = value; }
            get { return _torporW; }
        }

        private double _torporrate = 0;
        public double Torporrate
        {
            set { _torporrate = value; }
            get { return _torporrate; }
        }

        private double _baseaffinity = 0;
        public double Baseaffinity
        {
            set { _baseaffinity = value; }
            get { return _baseaffinity; }
        }

        private double _affinityperlvl = 0;
        public double Affinityperlvl
        {
            set { _affinityperlvl = value; }
            get { return _affinityperlvl; }
        }

        private string _basefood = "";
        public string Basefood
        {
            set { _basefood = value; }
            get { return _basefood; }
        }

        private string[] _foods = new string[1];
        public string[] Foods
        {
            set
            {
                _foods = new string[value.Length];
                value.CopyTo(_foods,0);
            }
            get { return _foods; }
        }

        private string _kibble = "";
        public string Kibble
        {
            set { _kibble = value; }
            get { return _kibble; }
        }

        private string _food1 = "";
        public string Food1
        {
            set { _food1 = value; }
            get { return _food1; }
        }

        private string _food2 = "";
        public string Food2
        {
            set { _food2 = value; }
            get { return _food2; }
        }

        private string _food3 = "";
        public string Food3
        {
            set { _food3 = value; }
            get { return _food3; }
        }

        private string _food4 = "";
        public string Food4
        {
            set { _food4 = value; }
            get { return _food4; }
        }

        private double _projectiledamage = 0;
        public double Projectiledamage
        {
            set { _projectiledamage = value; }
            get { return _projectiledamage; }
        }

        private double _hb_body = 0;
        public double Hb_body
        {
            set { _hb_body = value; }
            get { return _hb_body; }
        }

        private double _hb_head = 0;
        public double Hb_head
        {
            set { _hb_head = value; }
            get { return _hb_head; }
        }

        private double _hb_shell = 0;
        public double Hb_shell
        {
            set { _hb_shell = value; }
            get { return _hb_shell; }
        }

        private double _hb_tail = 0;
        public double Hb_tail
        {
            set { _hb_tail = value; }
            get { return _hb_tail; }
        }

        private Form1.TamingMethod _tamingmethods = Form1.TamingMethod.Standard;
        public Form1.TamingMethod Tamingmethods
        {
            set { _tamingmethods = value; }
            get { return _tamingmethods; }
        }

        private double _nonviolentfoodratemultiplier = 0;
        public double Nonviolentfoodratemultiplier
        {
            set { _nonviolentfoodratemultiplier = value; }
            get { return _nonviolentfoodratemultiplier; }
        }

        private double _nonviolentfoodaffinitymultiplier = 0;
        public double Nonviolentfoodaffinitymultiplier
        {
            set { _nonviolentfoodaffinitymultiplier = value; }
            get { return _nonviolentfoodaffinitymultiplier; }
        }
        #endregion

        private double _ineffectbyaff = 0;
        public double Ineffectbyaff
        {
            set { _ineffectbyaff = value; }
            get { return _ineffectbyaff; }
        }

        private double _effectiveness = 0;
        public double Effectiveness
        {
            set { _effectiveness = value; }
            get { return _effectiveness; }
        }

        private double _extralevels = 0;
        public double Extralevels
        {
            set { _extralevels = value; }
            get { return _extralevels; }
        }
    }
    #endregion
}
