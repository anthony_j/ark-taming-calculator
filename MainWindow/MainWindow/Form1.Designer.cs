﻿namespace MainWindow
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tab_tame = new System.Windows.Forms.TabPage();
            this.lbl_food_head = new System.Windows.Forms.Label();
            this.lbl_quantity_head = new System.Windows.Forms.Label();
            this.lbl_maximum_head = new System.Windows.Forms.Label();
            this.lbl_time_head = new System.Windows.Forms.Label();
            this.lbl_totalTime = new System.Windows.Forms.Label();
            this.lbl_effective = new System.Windows.Forms.Label();
            this.lbl_narco = new System.Windows.Forms.Label();
            this.lbl_targetFood = new System.Windows.Forms.Label();
            this.lbl_v_targetFood_ans = new System.Windows.Forms.Label();
            this.lbl_v_narco_ans = new System.Windows.Forms.Label();
            this.lbl_v_effective_ans = new System.Windows.Forms.Label();
            this.lbl_v_totalTime_ans = new System.Windows.Forms.Label();
            this.txt_food4_q = new System.Windows.Forms.TextBox();
            this.txt_food3_q = new System.Windows.Forms.TextBox();
            this.txt_food2_q = new System.Windows.Forms.TextBox();
            this.txt_food1_q = new System.Windows.Forms.TextBox();
            this.lbl_v_food4_time = new System.Windows.Forms.Label();
            this.lbl_v_food3_time = new System.Windows.Forms.Label();
            this.lbl_v_food2_time = new System.Windows.Forms.Label();
            this.lbl_v_food1_time = new System.Windows.Forms.Label();
            this.lbl_v_food4_max = new System.Windows.Forms.Label();
            this.lbl_v_food3_max = new System.Windows.Forms.Label();
            this.lbl_v_food2_max = new System.Windows.Forms.Label();
            this.lbl_v_food1_max = new System.Windows.Forms.Label();
            this.lbl_v_food1 = new System.Windows.Forms.Label();
            this.lbl_v_food2 = new System.Windows.Forms.Label();
            this.lbl_v_food3 = new System.Windows.Forms.Label();
            this.lbl_v_food4 = new System.Windows.Forms.Label();
            this.tab_down = new System.Windows.Forms.TabPage();
            this.lbl_stats = new System.Windows.Forms.Label();
            this.lbl_value = new System.Windows.Forms.Label();
            this.lbl_pointSpent = new System.Windows.Forms.Label();
            this.lbl_percentage = new System.Windows.Forms.Label();
            this.lbl_hp = new System.Windows.Forms.Label();
            this.lbl_sta = new System.Windows.Forms.Label();
            this.lbl_oxy = new System.Windows.Forms.Label();
            this.lbl_foo = new System.Windows.Forms.Label();
            this.lbl_wei = new System.Windows.Forms.Label();
            this.lbl_mel = new System.Windows.Forms.Label();
            this.lbl_mov = new System.Windows.Forms.Label();
            this.lbl_v_move = new System.Windows.Forms.Label();
            this.lbl_v_move_pc = new System.Windows.Forms.Label();
            this.lbl_v_move_p = new System.Windows.Forms.Label();
            this.txt_melee_q = new System.Windows.Forms.TextBox();
            this.txt_weight_q = new System.Windows.Forms.TextBox();
            this.txt_food_q = new System.Windows.Forms.TextBox();
            this.txt_oxygen_q = new System.Windows.Forms.TextBox();
            this.txt_stamina_q = new System.Windows.Forms.TextBox();
            this.txt_hp_q = new System.Windows.Forms.TextBox();
            this.lbl_v_melee_pc = new System.Windows.Forms.Label();
            this.lbl_v_weight_pc = new System.Windows.Forms.Label();
            this.lbl_v_food_pc = new System.Windows.Forms.Label();
            this.lbl_v_stamina_pc = new System.Windows.Forms.Label();
            this.lbl_v_oxygen_pc = new System.Windows.Forms.Label();
            this.lbl_v_hp_pc = new System.Windows.Forms.Label();
            this.lbl_v_melee_p = new System.Windows.Forms.Label();
            this.lbl_v_weight_p = new System.Windows.Forms.Label();
            this.lbl_v_food_p = new System.Windows.Forms.Label();
            this.lbl_v_oxygen_p = new System.Windows.Forms.Label();
            this.lbl_v_stamina_p = new System.Windows.Forms.Label();
            this.lbl_v_hp_p = new System.Windows.Forms.Label();
            this.tab_kibble = new System.Windows.Forms.TabPage();
            this.num_lvl = new System.Windows.Forms.NumericUpDown();
            this.cmb_creature = new System.Windows.Forms.ComboBox();
            this.lbl_lvl = new System.Windows.Forms.Label();
            this.lbl_creature = new System.Windows.Forms.Label();
            this.rb_eng = new System.Windows.Forms.RadioButton();
            this.rb_chs = new System.Windows.Forms.RadioButton();
            this.tabControl1.SuspendLayout();
            this.tab_tame.SuspendLayout();
            this.tab_down.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.num_lvl)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControl1.Controls.Add(this.tab_tame);
            this.tabControl1.Controls.Add(this.tab_down);
            this.tabControl1.Controls.Add(this.tab_kibble);
            this.tabControl1.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tabControl1.Location = new System.Drawing.Point(12, 94);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(615, 433);
            this.tabControl1.TabIndex = 0;
            // 
            // tab_tame
            // 
            this.tab_tame.Controls.Add(this.lbl_food_head);
            this.tab_tame.Controls.Add(this.lbl_quantity_head);
            this.tab_tame.Controls.Add(this.lbl_maximum_head);
            this.tab_tame.Controls.Add(this.lbl_time_head);
            this.tab_tame.Controls.Add(this.lbl_totalTime);
            this.tab_tame.Controls.Add(this.lbl_effective);
            this.tab_tame.Controls.Add(this.lbl_narco);
            this.tab_tame.Controls.Add(this.lbl_targetFood);
            this.tab_tame.Controls.Add(this.lbl_v_targetFood_ans);
            this.tab_tame.Controls.Add(this.lbl_v_narco_ans);
            this.tab_tame.Controls.Add(this.lbl_v_effective_ans);
            this.tab_tame.Controls.Add(this.lbl_v_totalTime_ans);
            this.tab_tame.Controls.Add(this.txt_food4_q);
            this.tab_tame.Controls.Add(this.txt_food3_q);
            this.tab_tame.Controls.Add(this.txt_food2_q);
            this.tab_tame.Controls.Add(this.txt_food1_q);
            this.tab_tame.Controls.Add(this.lbl_v_food4_time);
            this.tab_tame.Controls.Add(this.lbl_v_food3_time);
            this.tab_tame.Controls.Add(this.lbl_v_food2_time);
            this.tab_tame.Controls.Add(this.lbl_v_food1_time);
            this.tab_tame.Controls.Add(this.lbl_v_food4_max);
            this.tab_tame.Controls.Add(this.lbl_v_food3_max);
            this.tab_tame.Controls.Add(this.lbl_v_food2_max);
            this.tab_tame.Controls.Add(this.lbl_v_food1_max);
            this.tab_tame.Controls.Add(this.lbl_v_food1);
            this.tab_tame.Controls.Add(this.lbl_v_food2);
            this.tab_tame.Controls.Add(this.lbl_v_food3);
            this.tab_tame.Controls.Add(this.lbl_v_food4);
            this.tab_tame.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tab_tame.Location = new System.Drawing.Point(4, 29);
            this.tab_tame.Name = "tab_tame";
            this.tab_tame.Padding = new System.Windows.Forms.Padding(3);
            this.tab_tame.Size = new System.Drawing.Size(607, 400);
            this.tab_tame.TabIndex = 0;
            this.tab_tame.Text = "Taming Calculator";
            this.tab_tame.UseVisualStyleBackColor = true;
            // 
            // lbl_food_head
            // 
            this.lbl_food_head.AutoSize = true;
            this.lbl_food_head.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_food_head.Location = new System.Drawing.Point(41, 29);
            this.lbl_food_head.Name = "lbl_food_head";
            this.lbl_food_head.Size = new System.Drawing.Size(38, 17);
            this.lbl_food_head.TabIndex = 6;
            this.lbl_food_head.Text = "Food";
            // 
            // lbl_quantity_head
            // 
            this.lbl_quantity_head.AutoSize = true;
            this.lbl_quantity_head.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_quantity_head.Location = new System.Drawing.Point(207, 29);
            this.lbl_quantity_head.Name = "lbl_quantity_head";
            this.lbl_quantity_head.Size = new System.Drawing.Size(56, 17);
            this.lbl_quantity_head.TabIndex = 7;
            this.lbl_quantity_head.Text = "Quantity";
            // 
            // lbl_maximum_head
            // 
            this.lbl_maximum_head.AutoSize = true;
            this.lbl_maximum_head.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_maximum_head.Location = new System.Drawing.Point(366, 29);
            this.lbl_maximum_head.Name = "lbl_maximum_head";
            this.lbl_maximum_head.Size = new System.Drawing.Size(65, 17);
            this.lbl_maximum_head.TabIndex = 8;
            this.lbl_maximum_head.Text = "Maximum";
            // 
            // lbl_time_head
            // 
            this.lbl_time_head.AutoSize = true;
            this.lbl_time_head.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_time_head.Location = new System.Drawing.Point(478, 29);
            this.lbl_time_head.Name = "lbl_time_head";
            this.lbl_time_head.Size = new System.Drawing.Size(99, 17);
            this.lbl_time_head.TabIndex = 9;
            this.lbl_time_head.Text = "Time (Exclusive)";
            // 
            // lbl_totalTime
            // 
            this.lbl_totalTime.AutoSize = true;
            this.lbl_totalTime.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_totalTime.Location = new System.Drawing.Point(41, 254);
            this.lbl_totalTime.Name = "lbl_totalTime";
            this.lbl_totalTime.Size = new System.Drawing.Size(72, 17);
            this.lbl_totalTime.TabIndex = 20;
            this.lbl_totalTime.Text = "Total Time:";
            // 
            // lbl_effective
            // 
            this.lbl_effective.AutoSize = true;
            this.lbl_effective.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_effective.Location = new System.Drawing.Point(41, 290);
            this.lbl_effective.Name = "lbl_effective";
            this.lbl_effective.Size = new System.Drawing.Size(85, 17);
            this.lbl_effective.TabIndex = 21;
            this.lbl_effective.Text = "Effectiveness:";
            // 
            // lbl_narco
            // 
            this.lbl_narco.AutoSize = true;
            this.lbl_narco.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_narco.Location = new System.Drawing.Point(41, 326);
            this.lbl_narco.Name = "lbl_narco";
            this.lbl_narco.Size = new System.Drawing.Size(63, 17);
            this.lbl_narco.TabIndex = 22;
            this.lbl_narco.Text = "Narcotics";
            // 
            // lbl_targetFood
            // 
            this.lbl_targetFood.AutoSize = true;
            this.lbl_targetFood.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_targetFood.Location = new System.Drawing.Point(41, 362);
            this.lbl_targetFood.Name = "lbl_targetFood";
            this.lbl_targetFood.Size = new System.Drawing.Size(83, 17);
            this.lbl_targetFood.TabIndex = 24;
            this.lbl_targetFood.Text = "Target Food:";
            // 
            // lbl_v_targetFood_ans
            // 
            this.lbl_v_targetFood_ans.AutoSize = true;
            this.lbl_v_targetFood_ans.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_v_targetFood_ans.Location = new System.Drawing.Point(169, 362);
            this.lbl_v_targetFood_ans.Name = "lbl_v_targetFood_ans";
            this.lbl_v_targetFood_ans.Size = new System.Drawing.Size(128, 17);
            this.lbl_v_targetFood_ans.TabIndex = 25;
            this.lbl_v_targetFood_ans.Text = "lbl_v_targetFood_ans";
            // 
            // lbl_v_narco_ans
            // 
            this.lbl_v_narco_ans.AutoSize = true;
            this.lbl_v_narco_ans.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_v_narco_ans.Location = new System.Drawing.Point(169, 326);
            this.lbl_v_narco_ans.Name = "lbl_v_narco_ans";
            this.lbl_v_narco_ans.Size = new System.Drawing.Size(96, 17);
            this.lbl_v_narco_ans.TabIndex = 23;
            this.lbl_v_narco_ans.Text = "lbl_v_narco_ans";
            // 
            // lbl_v_effective_ans
            // 
            this.lbl_v_effective_ans.AutoSize = true;
            this.lbl_v_effective_ans.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_v_effective_ans.Location = new System.Drawing.Point(169, 290);
            this.lbl_v_effective_ans.Name = "lbl_v_effective_ans";
            this.lbl_v_effective_ans.Size = new System.Drawing.Size(111, 17);
            this.lbl_v_effective_ans.TabIndex = 23;
            this.lbl_v_effective_ans.Text = "lbl_v_effective_ans";
            // 
            // lbl_v_totalTime_ans
            // 
            this.lbl_v_totalTime_ans.AutoSize = true;
            this.lbl_v_totalTime_ans.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_v_totalTime_ans.Location = new System.Drawing.Point(169, 254);
            this.lbl_v_totalTime_ans.Name = "lbl_v_totalTime_ans";
            this.lbl_v_totalTime_ans.Size = new System.Drawing.Size(117, 17);
            this.lbl_v_totalTime_ans.TabIndex = 23;
            this.lbl_v_totalTime_ans.Text = "lbl_v_totalTime_ans";
            // 
            // txt_food4_q
            // 
            this.txt_food4_q.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt_food4_q.Location = new System.Drawing.Point(207, 174);
            this.txt_food4_q.Name = "txt_food4_q";
            this.txt_food4_q.Size = new System.Drawing.Size(100, 23);
            this.txt_food4_q.TabIndex = 19;
            this.txt_food4_q.Text = "txt_food4_q";
            // 
            // txt_food3_q
            // 
            this.txt_food3_q.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt_food3_q.Location = new System.Drawing.Point(207, 138);
            this.txt_food3_q.Name = "txt_food3_q";
            this.txt_food3_q.Size = new System.Drawing.Size(100, 23);
            this.txt_food3_q.TabIndex = 19;
            this.txt_food3_q.Text = "txt_food3_q";
            // 
            // txt_food2_q
            // 
            this.txt_food2_q.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt_food2_q.Location = new System.Drawing.Point(207, 102);
            this.txt_food2_q.Name = "txt_food2_q";
            this.txt_food2_q.Size = new System.Drawing.Size(100, 23);
            this.txt_food2_q.TabIndex = 19;
            this.txt_food2_q.Text = "txt_food2_q";
            // 
            // txt_food1_q
            // 
            this.txt_food1_q.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt_food1_q.Location = new System.Drawing.Point(207, 66);
            this.txt_food1_q.Name = "txt_food1_q";
            this.txt_food1_q.Size = new System.Drawing.Size(100, 23);
            this.txt_food1_q.TabIndex = 19;
            this.txt_food1_q.Text = "txt_food1_q";
            // 
            // lbl_v_food4_time
            // 
            this.lbl_v_food4_time.AutoSize = true;
            this.lbl_v_food4_time.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_v_food4_time.Location = new System.Drawing.Point(478, 177);
            this.lbl_v_food4_time.Name = "lbl_v_food4_time";
            this.lbl_v_food4_time.Size = new System.Drawing.Size(84, 17);
            this.lbl_v_food4_time.TabIndex = 17;
            this.lbl_v_food4_time.Text = "food4_v_time";
            // 
            // lbl_v_food3_time
            // 
            this.lbl_v_food3_time.AutoSize = true;
            this.lbl_v_food3_time.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_v_food3_time.Location = new System.Drawing.Point(478, 141);
            this.lbl_v_food3_time.Name = "lbl_v_food3_time";
            this.lbl_v_food3_time.Size = new System.Drawing.Size(84, 17);
            this.lbl_v_food3_time.TabIndex = 16;
            this.lbl_v_food3_time.Text = "food3_v_time";
            // 
            // lbl_v_food2_time
            // 
            this.lbl_v_food2_time.AutoSize = true;
            this.lbl_v_food2_time.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_v_food2_time.Location = new System.Drawing.Point(478, 105);
            this.lbl_v_food2_time.Name = "lbl_v_food2_time";
            this.lbl_v_food2_time.Size = new System.Drawing.Size(84, 17);
            this.lbl_v_food2_time.TabIndex = 15;
            this.lbl_v_food2_time.Text = "food2_v_time";
            // 
            // lbl_v_food1_time
            // 
            this.lbl_v_food1_time.AutoSize = true;
            this.lbl_v_food1_time.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_v_food1_time.Location = new System.Drawing.Point(478, 69);
            this.lbl_v_food1_time.Name = "lbl_v_food1_time";
            this.lbl_v_food1_time.Size = new System.Drawing.Size(84, 17);
            this.lbl_v_food1_time.TabIndex = 14;
            this.lbl_v_food1_time.Text = "food1_v_time";
            // 
            // lbl_v_food4_max
            // 
            this.lbl_v_food4_max.AutoSize = true;
            this.lbl_v_food4_max.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_v_food4_max.Location = new System.Drawing.Point(366, 177);
            this.lbl_v_food4_max.Name = "lbl_v_food4_max";
            this.lbl_v_food4_max.Size = new System.Drawing.Size(70, 17);
            this.lbl_v_food4_max.TabIndex = 13;
            this.lbl_v_food4_max.Text = "food4_v_m";
            // 
            // lbl_v_food3_max
            // 
            this.lbl_v_food3_max.AutoSize = true;
            this.lbl_v_food3_max.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_v_food3_max.Location = new System.Drawing.Point(366, 141);
            this.lbl_v_food3_max.Name = "lbl_v_food3_max";
            this.lbl_v_food3_max.Size = new System.Drawing.Size(70, 17);
            this.lbl_v_food3_max.TabIndex = 12;
            this.lbl_v_food3_max.Text = "food3_v_m";
            // 
            // lbl_v_food2_max
            // 
            this.lbl_v_food2_max.AutoSize = true;
            this.lbl_v_food2_max.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_v_food2_max.Location = new System.Drawing.Point(366, 105);
            this.lbl_v_food2_max.Name = "lbl_v_food2_max";
            this.lbl_v_food2_max.Size = new System.Drawing.Size(70, 17);
            this.lbl_v_food2_max.TabIndex = 11;
            this.lbl_v_food2_max.Text = "food2_v_m";
            // 
            // lbl_v_food1_max
            // 
            this.lbl_v_food1_max.AutoSize = true;
            this.lbl_v_food1_max.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_v_food1_max.Location = new System.Drawing.Point(366, 69);
            this.lbl_v_food1_max.Name = "lbl_v_food1_max";
            this.lbl_v_food1_max.Size = new System.Drawing.Size(70, 17);
            this.lbl_v_food1_max.TabIndex = 10;
            this.lbl_v_food1_max.Text = "food1_v_m";
            // 
            // lbl_v_food1
            // 
            this.lbl_v_food1.AutoSize = true;
            this.lbl_v_food1.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_v_food1.Location = new System.Drawing.Point(41, 69);
            this.lbl_v_food1.Name = "lbl_v_food1";
            this.lbl_v_food1.Size = new System.Drawing.Size(73, 17);
            this.lbl_v_food1.TabIndex = 2;
            this.lbl_v_food1.Text = "lbl_v_food1";
            // 
            // lbl_v_food2
            // 
            this.lbl_v_food2.AutoSize = true;
            this.lbl_v_food2.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_v_food2.Location = new System.Drawing.Point(41, 105);
            this.lbl_v_food2.Name = "lbl_v_food2";
            this.lbl_v_food2.Size = new System.Drawing.Size(73, 17);
            this.lbl_v_food2.TabIndex = 3;
            this.lbl_v_food2.Text = "lbl_v_food2";
            // 
            // lbl_v_food3
            // 
            this.lbl_v_food3.AutoSize = true;
            this.lbl_v_food3.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_v_food3.Location = new System.Drawing.Point(41, 141);
            this.lbl_v_food3.Name = "lbl_v_food3";
            this.lbl_v_food3.Size = new System.Drawing.Size(73, 17);
            this.lbl_v_food3.TabIndex = 4;
            this.lbl_v_food3.Text = "lbl_v_food3";
            // 
            // lbl_v_food4
            // 
            this.lbl_v_food4.AutoSize = true;
            this.lbl_v_food4.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_v_food4.ForeColor = System.Drawing.Color.Crimson;
            this.lbl_v_food4.Location = new System.Drawing.Point(41, 177);
            this.lbl_v_food4.Name = "lbl_v_food4";
            this.lbl_v_food4.Size = new System.Drawing.Size(73, 17);
            this.lbl_v_food4.TabIndex = 5;
            this.lbl_v_food4.Text = "lbl_v_food4";
            // 
            // tab_down
            // 
            this.tab_down.Controls.Add(this.lbl_stats);
            this.tab_down.Controls.Add(this.lbl_value);
            this.tab_down.Controls.Add(this.lbl_pointSpent);
            this.tab_down.Controls.Add(this.lbl_percentage);
            this.tab_down.Controls.Add(this.lbl_hp);
            this.tab_down.Controls.Add(this.lbl_sta);
            this.tab_down.Controls.Add(this.lbl_oxy);
            this.tab_down.Controls.Add(this.lbl_foo);
            this.tab_down.Controls.Add(this.lbl_wei);
            this.tab_down.Controls.Add(this.lbl_mel);
            this.tab_down.Controls.Add(this.lbl_mov);
            this.tab_down.Controls.Add(this.lbl_v_move);
            this.tab_down.Controls.Add(this.lbl_v_move_pc);
            this.tab_down.Controls.Add(this.lbl_v_move_p);
            this.tab_down.Controls.Add(this.txt_melee_q);
            this.tab_down.Controls.Add(this.txt_weight_q);
            this.tab_down.Controls.Add(this.txt_food_q);
            this.tab_down.Controls.Add(this.txt_oxygen_q);
            this.tab_down.Controls.Add(this.txt_stamina_q);
            this.tab_down.Controls.Add(this.txt_hp_q);
            this.tab_down.Controls.Add(this.lbl_v_melee_pc);
            this.tab_down.Controls.Add(this.lbl_v_weight_pc);
            this.tab_down.Controls.Add(this.lbl_v_food_pc);
            this.tab_down.Controls.Add(this.lbl_v_stamina_pc);
            this.tab_down.Controls.Add(this.lbl_v_oxygen_pc);
            this.tab_down.Controls.Add(this.lbl_v_hp_pc);
            this.tab_down.Controls.Add(this.lbl_v_melee_p);
            this.tab_down.Controls.Add(this.lbl_v_weight_p);
            this.tab_down.Controls.Add(this.lbl_v_food_p);
            this.tab_down.Controls.Add(this.lbl_v_oxygen_p);
            this.tab_down.Controls.Add(this.lbl_v_stamina_p);
            this.tab_down.Controls.Add(this.lbl_v_hp_p);
            this.tab_down.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tab_down.Location = new System.Drawing.Point(4, 29);
            this.tab_down.Name = "tab_down";
            this.tab_down.Size = new System.Drawing.Size(607, 400);
            this.tab_down.TabIndex = 2;
            this.tab_down.Text = "Downed Stats";
            this.tab_down.UseVisualStyleBackColor = true;
            // 
            // lbl_stats
            // 
            this.lbl_stats.AutoSize = true;
            this.lbl_stats.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_stats.Location = new System.Drawing.Point(44, 31);
            this.lbl_stats.Name = "lbl_stats";
            this.lbl_stats.Size = new System.Drawing.Size(43, 17);
            this.lbl_stats.TabIndex = 35;
            this.lbl_stats.Text = "4Stats";
            // 
            // lbl_value
            // 
            this.lbl_value.AutoSize = true;
            this.lbl_value.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_value.Location = new System.Drawing.Point(140, 31);
            this.lbl_value.Name = "lbl_value";
            this.lbl_value.Size = new System.Drawing.Size(47, 17);
            this.lbl_value.TabIndex = 36;
            this.lbl_value.Text = "3Value";
            // 
            // lbl_pointSpent
            // 
            this.lbl_pointSpent.AutoSize = true;
            this.lbl_pointSpent.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_pointSpent.Location = new System.Drawing.Point(294, 31);
            this.lbl_pointSpent.Name = "lbl_pointSpent";
            this.lbl_pointSpent.Size = new System.Drawing.Size(101, 17);
            this.lbl_pointSpent.TabIndex = 37;
            this.lbl_pointSpent.Text = "2Lvl point Spent";
            // 
            // lbl_percentage
            // 
            this.lbl_percentage.AutoSize = true;
            this.lbl_percentage.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_percentage.Location = new System.Drawing.Point(467, 31);
            this.lbl_percentage.Name = "lbl_percentage";
            this.lbl_percentage.Size = new System.Drawing.Size(80, 17);
            this.lbl_percentage.TabIndex = 38;
            this.lbl_percentage.Text = "1Percentage";
            // 
            // lbl_hp
            // 
            this.lbl_hp.AutoSize = true;
            this.lbl_hp.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_hp.Location = new System.Drawing.Point(44, 67);
            this.lbl_hp.Name = "lbl_hp";
            this.lbl_hp.Size = new System.Drawing.Size(42, 17);
            this.lbl_hp.TabIndex = 31;
            this.lbl_hp.Text = "10Hp:";
            // 
            // lbl_sta
            // 
            this.lbl_sta.AutoSize = true;
            this.lbl_sta.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_sta.Location = new System.Drawing.Point(44, 108);
            this.lbl_sta.Name = "lbl_sta";
            this.lbl_sta.Size = new System.Drawing.Size(64, 17);
            this.lbl_sta.TabIndex = 32;
            this.lbl_sta.Text = "9Stamina:";
            // 
            // lbl_oxy
            // 
            this.lbl_oxy.AutoSize = true;
            this.lbl_oxy.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_oxy.Location = new System.Drawing.Point(44, 149);
            this.lbl_oxy.Name = "lbl_oxy";
            this.lbl_oxy.Size = new System.Drawing.Size(62, 17);
            this.lbl_oxy.TabIndex = 33;
            this.lbl_oxy.Text = "8Oxygen:";
            // 
            // lbl_foo
            // 
            this.lbl_foo.AutoSize = true;
            this.lbl_foo.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_foo.Location = new System.Drawing.Point(44, 190);
            this.lbl_foo.Name = "lbl_foo";
            this.lbl_foo.Size = new System.Drawing.Size(48, 17);
            this.lbl_foo.TabIndex = 34;
            this.lbl_foo.Text = "7Food:";
            // 
            // lbl_wei
            // 
            this.lbl_wei.AutoSize = true;
            this.lbl_wei.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_wei.Location = new System.Drawing.Point(44, 231);
            this.lbl_wei.Name = "lbl_wei";
            this.lbl_wei.Size = new System.Drawing.Size(59, 17);
            this.lbl_wei.TabIndex = 34;
            this.lbl_wei.Text = "6Weight:";
            // 
            // lbl_mel
            // 
            this.lbl_mel.AutoSize = true;
            this.lbl_mel.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_mel.Location = new System.Drawing.Point(44, 272);
            this.lbl_mel.Name = "lbl_mel";
            this.lbl_mel.Size = new System.Drawing.Size(54, 17);
            this.lbl_mel.TabIndex = 34;
            this.lbl_mel.Text = "5Melee:";
            // 
            // lbl_mov
            // 
            this.lbl_mov.AutoSize = true;
            this.lbl_mov.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_mov.Location = new System.Drawing.Point(44, 313);
            this.lbl_mov.Name = "lbl_mov";
            this.lbl_mov.Size = new System.Drawing.Size(100, 17);
            this.lbl_mov.TabIndex = 51;
            this.lbl_mov.Text = "Movement Spd:";
            // 
            // lbl_v_move
            // 
            this.lbl_v_move.AutoSize = true;
            this.lbl_v_move.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_v_move.Location = new System.Drawing.Point(140, 313);
            this.lbl_v_move.Name = "lbl_v_move";
            this.lbl_v_move.Size = new System.Drawing.Size(40, 17);
            this.lbl_v_move.TabIndex = 51;
            this.lbl_v_move.Text = "100%";
            // 
            // lbl_v_move_pc
            // 
            this.lbl_v_move_pc.AutoSize = true;
            this.lbl_v_move_pc.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_v_move_pc.Location = new System.Drawing.Point(467, 313);
            this.lbl_v_move_pc.Name = "lbl_v_move_pc";
            this.lbl_v_move_pc.Size = new System.Drawing.Size(89, 17);
            this.lbl_v_move_pc.TabIndex = 51;
            this.lbl_v_move_pc.Text = "lbl_v_move_pc";
            // 
            // lbl_v_move_p
            // 
            this.lbl_v_move_p.AutoSize = true;
            this.lbl_v_move_p.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_v_move_p.Location = new System.Drawing.Point(294, 313);
            this.lbl_v_move_p.Name = "lbl_v_move_p";
            this.lbl_v_move_p.Size = new System.Drawing.Size(83, 17);
            this.lbl_v_move_p.TabIndex = 51;
            this.lbl_v_move_p.Text = "lbl_v_move_p";
            // 
            // txt_melee_q
            // 
            this.txt_melee_q.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt_melee_q.Location = new System.Drawing.Point(140, 268);
            this.txt_melee_q.Name = "txt_melee_q";
            this.txt_melee_q.Size = new System.Drawing.Size(100, 23);
            this.txt_melee_q.TabIndex = 50;
            this.txt_melee_q.Text = "txt_melee_q";
            // 
            // txt_weight_q
            // 
            this.txt_weight_q.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt_weight_q.Location = new System.Drawing.Point(140, 227);
            this.txt_weight_q.Name = "txt_weight_q";
            this.txt_weight_q.Size = new System.Drawing.Size(100, 23);
            this.txt_weight_q.TabIndex = 50;
            this.txt_weight_q.Text = "txt_weight_q";
            // 
            // txt_food_q
            // 
            this.txt_food_q.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt_food_q.Location = new System.Drawing.Point(140, 186);
            this.txt_food_q.Name = "txt_food_q";
            this.txt_food_q.Size = new System.Drawing.Size(100, 23);
            this.txt_food_q.TabIndex = 50;
            this.txt_food_q.Text = "txt_food_q";
            // 
            // txt_oxygen_q
            // 
            this.txt_oxygen_q.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt_oxygen_q.Location = new System.Drawing.Point(140, 145);
            this.txt_oxygen_q.Name = "txt_oxygen_q";
            this.txt_oxygen_q.Size = new System.Drawing.Size(100, 23);
            this.txt_oxygen_q.TabIndex = 49;
            this.txt_oxygen_q.Text = "txt_oxygen_q";
            // 
            // txt_stamina_q
            // 
            this.txt_stamina_q.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt_stamina_q.Location = new System.Drawing.Point(140, 104);
            this.txt_stamina_q.Name = "txt_stamina_q";
            this.txt_stamina_q.Size = new System.Drawing.Size(100, 23);
            this.txt_stamina_q.TabIndex = 48;
            this.txt_stamina_q.Text = "txt_stamina_q";
            // 
            // txt_hp_q
            // 
            this.txt_hp_q.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt_hp_q.Location = new System.Drawing.Point(140, 63);
            this.txt_hp_q.Name = "txt_hp_q";
            this.txt_hp_q.Size = new System.Drawing.Size(100, 23);
            this.txt_hp_q.TabIndex = 47;
            this.txt_hp_q.Text = "txt_hp_q";
            // 
            // lbl_v_melee_pc
            // 
            this.lbl_v_melee_pc.AutoSize = true;
            this.lbl_v_melee_pc.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_v_melee_pc.Location = new System.Drawing.Point(467, 272);
            this.lbl_v_melee_pc.Name = "lbl_v_melee_pc";
            this.lbl_v_melee_pc.Size = new System.Drawing.Size(92, 17);
            this.lbl_v_melee_pc.TabIndex = 46;
            this.lbl_v_melee_pc.Text = "lbl_v_melee_pc";
            // 
            // lbl_v_weight_pc
            // 
            this.lbl_v_weight_pc.AutoSize = true;
            this.lbl_v_weight_pc.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_v_weight_pc.Location = new System.Drawing.Point(467, 231);
            this.lbl_v_weight_pc.Name = "lbl_v_weight_pc";
            this.lbl_v_weight_pc.Size = new System.Drawing.Size(95, 17);
            this.lbl_v_weight_pc.TabIndex = 46;
            this.lbl_v_weight_pc.Text = "lbl_v_weight_pc";
            // 
            // lbl_v_food_pc
            // 
            this.lbl_v_food_pc.AutoSize = true;
            this.lbl_v_food_pc.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_v_food_pc.Location = new System.Drawing.Point(467, 190);
            this.lbl_v_food_pc.Name = "lbl_v_food_pc";
            this.lbl_v_food_pc.Size = new System.Drawing.Size(85, 17);
            this.lbl_v_food_pc.TabIndex = 46;
            this.lbl_v_food_pc.Text = "lbl_v_food_pc";
            // 
            // lbl_v_stamina_pc
            // 
            this.lbl_v_stamina_pc.AutoSize = true;
            this.lbl_v_stamina_pc.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_v_stamina_pc.Location = new System.Drawing.Point(467, 108);
            this.lbl_v_stamina_pc.Name = "lbl_v_stamina_pc";
            this.lbl_v_stamina_pc.Size = new System.Drawing.Size(102, 17);
            this.lbl_v_stamina_pc.TabIndex = 45;
            this.lbl_v_stamina_pc.Text = "lbl_v_stamina_pc";
            // 
            // lbl_v_oxygen_pc
            // 
            this.lbl_v_oxygen_pc.AutoSize = true;
            this.lbl_v_oxygen_pc.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_v_oxygen_pc.Location = new System.Drawing.Point(467, 149);
            this.lbl_v_oxygen_pc.Name = "lbl_v_oxygen_pc";
            this.lbl_v_oxygen_pc.Size = new System.Drawing.Size(99, 17);
            this.lbl_v_oxygen_pc.TabIndex = 44;
            this.lbl_v_oxygen_pc.Text = "lbl_v_oxygen_pc";
            // 
            // lbl_v_hp_pc
            // 
            this.lbl_v_hp_pc.AutoSize = true;
            this.lbl_v_hp_pc.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_v_hp_pc.Location = new System.Drawing.Point(467, 67);
            this.lbl_v_hp_pc.Name = "lbl_v_hp_pc";
            this.lbl_v_hp_pc.Size = new System.Drawing.Size(72, 17);
            this.lbl_v_hp_pc.TabIndex = 43;
            this.lbl_v_hp_pc.Text = "lbl_v_hp_pc";
            // 
            // lbl_v_melee_p
            // 
            this.lbl_v_melee_p.AutoSize = true;
            this.lbl_v_melee_p.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_v_melee_p.Location = new System.Drawing.Point(294, 272);
            this.lbl_v_melee_p.Name = "lbl_v_melee_p";
            this.lbl_v_melee_p.Size = new System.Drawing.Size(86, 17);
            this.lbl_v_melee_p.TabIndex = 42;
            this.lbl_v_melee_p.Text = "lbl_v_melee_p";
            // 
            // lbl_v_weight_p
            // 
            this.lbl_v_weight_p.AutoSize = true;
            this.lbl_v_weight_p.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_v_weight_p.Location = new System.Drawing.Point(294, 231);
            this.lbl_v_weight_p.Name = "lbl_v_weight_p";
            this.lbl_v_weight_p.Size = new System.Drawing.Size(89, 17);
            this.lbl_v_weight_p.TabIndex = 42;
            this.lbl_v_weight_p.Text = "lbl_v_weight_p";
            // 
            // lbl_v_food_p
            // 
            this.lbl_v_food_p.AutoSize = true;
            this.lbl_v_food_p.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_v_food_p.Location = new System.Drawing.Point(294, 190);
            this.lbl_v_food_p.Name = "lbl_v_food_p";
            this.lbl_v_food_p.Size = new System.Drawing.Size(79, 17);
            this.lbl_v_food_p.TabIndex = 42;
            this.lbl_v_food_p.Text = "lbl_v_food_p";
            // 
            // lbl_v_oxygen_p
            // 
            this.lbl_v_oxygen_p.AutoSize = true;
            this.lbl_v_oxygen_p.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_v_oxygen_p.Location = new System.Drawing.Point(294, 149);
            this.lbl_v_oxygen_p.Name = "lbl_v_oxygen_p";
            this.lbl_v_oxygen_p.Size = new System.Drawing.Size(93, 17);
            this.lbl_v_oxygen_p.TabIndex = 41;
            this.lbl_v_oxygen_p.Text = "lbl_v_oxygen_p";
            // 
            // lbl_v_stamina_p
            // 
            this.lbl_v_stamina_p.AutoSize = true;
            this.lbl_v_stamina_p.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_v_stamina_p.Location = new System.Drawing.Point(294, 108);
            this.lbl_v_stamina_p.Name = "lbl_v_stamina_p";
            this.lbl_v_stamina_p.Size = new System.Drawing.Size(96, 17);
            this.lbl_v_stamina_p.TabIndex = 40;
            this.lbl_v_stamina_p.Text = "lbl_v_stamina_p";
            // 
            // lbl_v_hp_p
            // 
            this.lbl_v_hp_p.AutoSize = true;
            this.lbl_v_hp_p.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_v_hp_p.Location = new System.Drawing.Point(294, 67);
            this.lbl_v_hp_p.Name = "lbl_v_hp_p";
            this.lbl_v_hp_p.Size = new System.Drawing.Size(66, 17);
            this.lbl_v_hp_p.TabIndex = 39;
            this.lbl_v_hp_p.Text = "lbl_v_hp_p";
            // 
            // tab_kibble
            // 
            this.tab_kibble.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tab_kibble.Location = new System.Drawing.Point(4, 29);
            this.tab_kibble.Name = "tab_kibble";
            this.tab_kibble.Padding = new System.Windows.Forms.Padding(3);
            this.tab_kibble.Size = new System.Drawing.Size(607, 400);
            this.tab_kibble.TabIndex = 1;
            this.tab_kibble.Text = "Kibble";
            this.tab_kibble.UseVisualStyleBackColor = true;
            // 
            // num_lvl
            // 
            this.num_lvl.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.num_lvl.Location = new System.Drawing.Point(158, 57);
            this.num_lvl.Maximum = new decimal(new int[] {
            120,
            0,
            0,
            0});
            this.num_lvl.Name = "num_lvl";
            this.num_lvl.Size = new System.Drawing.Size(68, 23);
            this.num_lvl.TabIndex = 26;
            this.num_lvl.ValueChanged += new System.EventHandler(this.num_lvl_ValueChanged);
            // 
            // cmb_creature
            // 
            this.cmb_creature.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmb_creature.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cmb_creature.FormattingEnabled = true;
            this.cmb_creature.Location = new System.Drawing.Point(158, 22);
            this.cmb_creature.Name = "cmb_creature";
            this.cmb_creature.Size = new System.Drawing.Size(203, 25);
            this.cmb_creature.TabIndex = 18;
            this.cmb_creature.SelectedIndexChanged += new System.EventHandler(this.cmb_creature_SelectedIndexChanged);
            // 
            // lbl_lvl
            // 
            this.lbl_lvl.AutoSize = true;
            this.lbl_lvl.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_lvl.Location = new System.Drawing.Point(27, 61);
            this.lbl_lvl.Name = "lbl_lvl";
            this.lbl_lvl.Size = new System.Drawing.Size(40, 17);
            this.lbl_lvl.TabIndex = 1;
            this.lbl_lvl.Text = "Level:";
            // 
            // lbl_creature
            // 
            this.lbl_creature.AutoSize = true;
            this.lbl_creature.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_creature.Location = new System.Drawing.Point(27, 26);
            this.lbl_creature.Name = "lbl_creature";
            this.lbl_creature.Size = new System.Drawing.Size(65, 17);
            this.lbl_creature.TabIndex = 0;
            this.lbl_creature.Text = "Creature: ";
            // 
            // rb_eng
            // 
            this.rb_eng.AutoSize = true;
            this.rb_eng.Checked = true;
            this.rb_eng.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.rb_eng.Location = new System.Drawing.Point(538, 11);
            this.rb_eng.Name = "rb_eng";
            this.rb_eng.Size = new System.Drawing.Size(43, 21);
            this.rb_eng.TabIndex = 1;
            this.rb_eng.TabStop = true;
            this.rb_eng.Text = "EN";
            this.rb_eng.UseVisualStyleBackColor = true;
            this.rb_eng.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // rb_chs
            // 
            this.rb_chs.AutoSize = true;
            this.rb_chs.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.rb_chs.Location = new System.Drawing.Point(584, 11);
            this.rb_chs.Name = "rb_chs";
            this.rb_chs.Size = new System.Drawing.Size(50, 21);
            this.rb_chs.TabIndex = 2;
            this.rb_chs.Text = "中文";
            this.rb_chs.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(639, 543);
            this.Controls.Add(this.num_lvl);
            this.Controls.Add(this.rb_chs);
            this.Controls.Add(this.rb_eng);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.cmb_creature);
            this.Controls.Add(this.lbl_creature);
            this.Controls.Add(this.lbl_lvl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tab_tame.ResumeLayout(false);
            this.tab_tame.PerformLayout();
            this.tab_down.ResumeLayout(false);
            this.tab_down.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.num_lvl)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tab_tame;
        private System.Windows.Forms.NumericUpDown num_lvl;
        private System.Windows.Forms.Label lbl_v_targetFood_ans;
        private System.Windows.Forms.Label lbl_targetFood;
        private System.Windows.Forms.Label lbl_v_narco_ans;
        private System.Windows.Forms.Label lbl_v_effective_ans;
        private System.Windows.Forms.Label lbl_v_totalTime_ans;
        private System.Windows.Forms.Label lbl_narco;
        private System.Windows.Forms.Label lbl_effective;
        private System.Windows.Forms.Label lbl_totalTime;
        private System.Windows.Forms.TextBox txt_food4_q;
        private System.Windows.Forms.TextBox txt_food3_q;
        private System.Windows.Forms.TextBox txt_food2_q;
        private System.Windows.Forms.TextBox txt_food1_q;
        private System.Windows.Forms.ComboBox cmb_creature;
        private System.Windows.Forms.Label lbl_v_food4_time;
        private System.Windows.Forms.Label lbl_v_food3_time;
        private System.Windows.Forms.Label lbl_v_food2_time;
        private System.Windows.Forms.Label lbl_v_food1_time;
        private System.Windows.Forms.Label lbl_v_food4_max;
        private System.Windows.Forms.Label lbl_v_food3_max;
        private System.Windows.Forms.Label lbl_v_food2_max;
        private System.Windows.Forms.Label lbl_v_food1_max;
        private System.Windows.Forms.Label lbl_time_head;
        private System.Windows.Forms.Label lbl_maximum_head;
        private System.Windows.Forms.Label lbl_quantity_head;
        private System.Windows.Forms.Label lbl_food_head;
        private System.Windows.Forms.Label lbl_v_food4;
        private System.Windows.Forms.Label lbl_v_food3;
        private System.Windows.Forms.Label lbl_v_food2;
        private System.Windows.Forms.Label lbl_v_food1;
        private System.Windows.Forms.Label lbl_lvl;
        private System.Windows.Forms.Label lbl_creature;
        private System.Windows.Forms.TabPage tab_down;
        private System.Windows.Forms.TextBox txt_melee_q;
        private System.Windows.Forms.TextBox txt_weight_q;
        private System.Windows.Forms.TextBox txt_food_q;
        private System.Windows.Forms.TextBox txt_oxygen_q;
        private System.Windows.Forms.TextBox txt_stamina_q;
        private System.Windows.Forms.TextBox txt_hp_q;
        private System.Windows.Forms.Label lbl_v_food_pc;
        private System.Windows.Forms.Label lbl_v_stamina_pc;
        private System.Windows.Forms.Label lbl_v_oxygen_pc;
        private System.Windows.Forms.Label lbl_v_hp_pc;
        private System.Windows.Forms.Label lbl_v_melee_p;
        private System.Windows.Forms.Label lbl_v_weight_p;
        private System.Windows.Forms.Label lbl_v_food_p;
        private System.Windows.Forms.Label lbl_v_oxygen_p;
        private System.Windows.Forms.Label lbl_v_stamina_p;
        private System.Windows.Forms.Label lbl_v_hp_p;
        private System.Windows.Forms.Label lbl_percentage;
        private System.Windows.Forms.Label lbl_pointSpent;
        private System.Windows.Forms.Label lbl_value;
        private System.Windows.Forms.Label lbl_stats;
        private System.Windows.Forms.Label lbl_mel;
        private System.Windows.Forms.Label lbl_wei;
        private System.Windows.Forms.Label lbl_foo;
        private System.Windows.Forms.Label lbl_oxy;
        private System.Windows.Forms.Label lbl_sta;
        private System.Windows.Forms.Label lbl_hp;
        private System.Windows.Forms.TabPage tab_kibble;
        private System.Windows.Forms.RadioButton rb_eng;
        private System.Windows.Forms.RadioButton rb_chs;
        private System.Windows.Forms.Label lbl_v_melee_pc;
        private System.Windows.Forms.Label lbl_v_weight_pc;
        private System.Windows.Forms.Label lbl_v_move_pc;
        private System.Windows.Forms.Label lbl_v_move_p;
        private System.Windows.Forms.Label lbl_v_move;
        private System.Windows.Forms.Label lbl_mov;
    }
}

